terraform {
  source = "${include.root.locals.source_url}//modules/main/RDS?ref=${include.root.locals.source_version}"
}


include "root" {
  path   = find_in_parent_folders()
  expose = true
}

dependency "VPC" {
  config_path                             = "../networking/VPC/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "providers", "terragrunt-info", "show"]
  mock_outputs = {
    vpc_id         = "fake-vpc-id"
    vpc_cidr_block = "10.0.0.0/16"
    DataBase_ids    = []
  }
}

dependency "sg" {
  config_path                             = "../networking/S_Group/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "providers", "terragrunt-info", "show"]
  mock_outputs = {
    Securitygroup_id = "fake-Securitygroup-id"
  }
}

inputs = {
  privat_ids             = dependency.VPC.outputs.DataBase_ids
  vpc_security_group_ids = dependency.SG.outputs.Securitygroup_id
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "aurora-mysql"
  engine_version         = "5.7.mysql_aurora.2.03.2"
  instance_class         = "db.t2.micro"
}
