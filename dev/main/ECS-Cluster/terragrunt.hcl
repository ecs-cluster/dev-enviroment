terraform {
  source = "${include.root.locals.source_url}//modules/main/ECS-Cluster?ref=${include.root.locals.source_version}"
}


include "root" {
  path   = find_in_parent_folders()
  expose = true
}

dependency "VPC" {
  config_path                             = "../networking/VPC/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "providers", "terragrunt-info", "show"]
  mock_outputs = {
    vpc_id         = "fake-vpc-id"
    vpc_cidr_block = "10.0.0.0/16"
    Public_ids     = ["10.0.10.0/24", "10.0.16.0/24"]
  }
}

dependency "SG" {
  config_path                             = "../networking/S_Group/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "providers", "terragrunt-info", "show"]
  mock_outputs = {
    Securitygroup_id = "fake-Securitygroup-id"
  }
}
dependency "lb" {
  config_path                             = "../lb/"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "providers", "terragrunt-info", "show"]
  mock_outputs = {
    target_group_arn     = "fake-target-group-arn"
    target_groupapp2_arn = "fake-target-groupapp2-arn"
    target_groupapp3_arn = "fake-target-groupapp3-arn"
  }
}

dependency "ASG" {
  config_path                             = "../AutoSG"
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "providers", "terragrunt-info", "show"]
  mock_outputs = {
    auto_scaling_group_arn = "fake-AG-arn"
  }
}

inputs = {
  auto_scaling_group_arn = dependency.ASG.outputs.AG_arn
  subnets                = dependency.VPC.outputs.Public_ids
  security_group         = dependency.SG.outputs.Securitygroup_id
  target_group_arn       = dependency.lb.outputs.target_group_arn
  target_groupapp2_arn   = dependency.lb.outputstarget_groupapp2_arn
  target_groupapp3_arn   = dependency.lb.outputs.target_groupapp3_arn
}
